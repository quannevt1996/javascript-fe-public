const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const baseConfig = {
    mode: 'development',
    devtool: 'inline-source-map',
};

let service = merge(common, {
    entry: {
        signin: './src/serviceWorker.js',
    },
    output: {
        filename: './serviceWorker.js',
        path: path.resolve(__dirname, 'dist'),
    },
    ...baseConfig,
});

module.exports = [merge(common, {
    entry: {
        signup: './src/signup.js',
        signin: './src/signin.js',
        index: './src/index.js',
    },
    output: {
        filename: './[name]/bundle.js',
        path: path.resolve(__dirname, `dist`),
    },
    devServer: {
        contentBase: `./dist`,
    },
    ...baseConfig,
}), service];