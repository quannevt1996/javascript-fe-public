# Iptp Test




**My successful command to run docker :**  
	+ sudo docker build -t img-iptp -f Dockerfile .  
	+ sudo docker run -it --name mIptp -p 8888:8080 img-iptp


**Feature**:  
	+ All basic feature mentioned in test file requirement  
	+ Log out + token expired function  
	+ Automatic login or not if you choose if the token still have time remain  
	+ All data encrypted based on my custom algorithm  


**In case you want to build seperately:**  
	+ Clone from this repositiory  
	+ Run **npm i** and **npm run build**  
	+ Go to **dist** directory and host the **index.html** file  


**In case you want to test service worker on real host**  
	+ Run from this site (I've already hosted): [link demo](https://iptp-javascript.web.app/)


**Note:**  
	+ Token will expire in 60 minutes (can set in code) and you will be logged out if you don't create any new note between the time.  
	+ Please consider to use localhost or https to use service worker (cache all data)  