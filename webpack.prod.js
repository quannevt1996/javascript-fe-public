const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');
//
const baseConfig = {
    mode: 'production',
    plugins: [
        // new CleanWebpackPlugin(),
    ],
};
//
let service = merge(common, {
    entry: {
        signin: './src/serviceWorker.js',
    },
    output: {
        filename: './serviceWorker.js',
        path: path.resolve(__dirname, 'dist'),
    },
    ...baseConfig,
});
//
module.exports = [merge(common, {
    mode: 'production',
    entry: {
        signup: './src/signup.js',
        signin: './src/signin.js',
        index: './src/index.js',
    },
    output: {
        filename: './[name]/bundle.js',
        path: path.resolve(__dirname, 'dist'),
    },
    ...baseConfig,
}), service];