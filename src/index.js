import _ from 'lodash';
import './style.css';
import './index.css';
import './modal.css';
import * as services from './services';

if (process.env.NODE_ENV !== 'production') 
{
    console.warn('Looks like we are in development mode!');
}
else
{
    console.warn("Production mode, console cleared");
    console.log = function(){};
}

var isWorkerRegistered = false;
var cachedFunc = [];
var btnOpenDialog = document.getElementById('btnCreateNote');
var btnSubmitNote = document.getElementById('btnSubmitNote');
var btnLogout = document.getElementById('btnLogout');
var user = {};
btnOpenDialog.onclick = (f, e) => { 
    openDialog('createNoteModal');
};
btnSubmitNote.onclick = (f, e) => {
    submitNote(document.getElementById('noteContent').value);
    document.getElementById('modalClose').click();
};
btnLogout.onclick = (f, e) => {
    logOut();
    // let res = services.getUser(localStorage.getItem('token'));
    location.href = '/signin/';
}

function Initialize()
{
    // const element = document.createElement('div');
    // element.innerHTML = 'hello webpack';
    // document.body.appendChild(element);
    if (localStorage.getItem('token'))
    {
        let res = services.getUser(localStorage.getItem('token'));
        if (res.code == 401)
        {
            location.href = '/signin/';
        }
        else if (res.code == 200)
        {
            user = res.data;
            renderUI();
        }
    }
    else
    {
        location.href = '/signin/';
    }
}

function registerWorker()
{
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('../serviceWorker.js')
        .then((reg) => {
            // registration worked
            console.log('Registration succeeded. Scope is ' + reg.scope);
            isWorkerRegistered = true;
            if (cachedFunc.length > 0)
            {
                cachedFunc.forEach((f) => {
                    f && f();
                });
                cachedFunc.length = 0;
            }
            // fetchData();
        }).catch((error) => {
            // registration failed
            console.log('Registration failed with ' + error);
        });
    }
}

function renderUI()
{
    document.getElementById('username').innerHTML = user.name;
    let tableBody = document.getElementById('tableBody');
    if (!user.notes || user.notes.length == 0)
        return;
    for (let i = tableBody.children.length; i< user.notes.length; i++)
    {
        let row = document.createElement('tr');
        let c1 = document.createElement('td');
        let c2 = document.createElement('td');
        let date = new Date(user.notes[i].date);
        c1.innerHTML = `${date.getDay()}/${date.getMonth()}/${date.getFullYear()}`;
        c2.innerHTML = user.notes[i].content;
        row.appendChild(c1);
        row.appendChild(c2);
        tableBody.appendChild(row);
    }
}

function submitNote(content)
{
    let res = services.pushNote(localStorage.getItem('token'), content);
    if (res.code == 401)
    {

    }
    else if (res.code == 200)
    {
        user = res.data;
        renderUI();
    }
}

function logOut()
{
    services.logOut(localStorage.getItem('token'));
    localStorage.removeItem('token');
}

function openDialog(name)
{
    console.log('click');
    let modal = document.getElementById(name);
    modal.style.display = "block";
}

window.addEventListener('load', (event) => {
    console.log('page is fully loaded');
    Initialize();
    // fetchData();
    registerWorker();
});