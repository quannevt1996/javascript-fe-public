import _ from 'lodash';
import './style.css';
import './modal.css';
import * as services from './services';

if (process.env.NODE_ENV !== 'production') 
{
    console.warn('Looks like we are in development mode!');
}
else
{
    console.warn("Production mode, console cleared");
    console.log = function(){};
}
//
document.getElementById('btnSignIn').onclick = signIn;
document.getElementById('btnContinueSignin').onclick = signInSucess;
document.getElementById('btnResetSignIn').onclick = declineSignIn;

var isWorkerRegistered = false;
var cachedFunc = [];

function Initialize()
{
    // const element = document.createElement('div');
    // element.innerHTML = 'hello webpack';
    // document.body.appendChild(element);
    if (localStorage.getItem('token'))
    {
        let res = services.getLogin(localStorage.getItem('token'));
        // console.log(res)
        if (res.code == 401)
        {

        }
        else if (res.code == 200)
        {
            // signInSucess(res);
            localStorage.setItem('token', res.data.token);
            document.getElementById('accountName').innerHTML = res.data.userName;
            openDialog('continueLogin');
        }
    }
}

function registerWorker()
{
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('../serviceWorker.js')
        .then((reg) => {
            // registration worked
            console.log('Registration succeeded. Scope is ' + reg.scope);
            isWorkerRegistered = true;
            if (cachedFunc.length > 0)
            {
                cachedFunc.forEach((f) => {
                    f && f();
                });
                cachedFunc.length = 0;
            }
            // fetchData();
        }).catch((error) => {
            // registration failed
            console.log('Registration failed with ' + error);
        });
    }
}

function signIn()
{
    let userName = document.getElementById('username').value;
    let password = document.getElementById('password').value;
    if (userName == '' || password == '')
    {
        document.getElementById('signInFailContent').innerHTML = 'please enter a valid username or password';
        openDialog('signInFail');
    }
    else
    {
        let res = services.logIn(userName, password);
        if (res.code == 401)
        {
            // alert(res.message);
            document.getElementById('signInFailContent').innerHTML = res.message;
            openDialog('signInFail');
        }
        else 
        {
            localStorage.setItem('token', res.data.token);
            signInSucess();
        }
    }
}

function signInSucess()
{
    // alert('sign in successfully');
    location.href = '/index/';
    document.getElementById('continueLogin').style.display = 'none'
}

function declineSignIn()
{
    services.logOut(localStorage.getItem('token'));
    localStorage.removeItem('token');
    document.getElementById('continueLogin').style.display = 'none';
}

function openDialog(name)
{
    console.log('click');
    let modal = document.getElementById(name);
    modal.style.display = "block";
}

window.addEventListener('load', (event) => {
    console.log('page is fully loaded');
    Initialize();
    // fetchData();
    registerWorker();
    document.onkeypress = function(e){
        if (e.keyCode == 13){
            document.getElementById('btnSignIn').click();
        }
    };
});