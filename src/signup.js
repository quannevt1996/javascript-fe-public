import _ from 'lodash';
import './style.css';
import * as services from './services';
import './modal.css';

if (process.env.NODE_ENV !== 'production') 
{
    console.warn('Looks like we are in development mode!');
}
else
{
    console.warn("Production mode, console cleared");
    console.log = function(){};
}
//

var isWorkerRegistered = false;
var cachedFunc = [];
document.getElementById('btnSignUp').onclick = signUp;
// document.getElementById('test').onclick = function(){
//     console.log(helper.decryption(localStorage.getItem('data')))
// };

function Initialize()
{
    // const element = document.createElement('div');
    // element.innerHTML = 'hello webpack';
    // document.body.appendChild(element);
}

function registerWorker()
{
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('../serviceWorker.js')
        .then((reg) => {
            // registration worked
            console.log('SW registered!. Scope is ' + reg.scope);
            isWorkerRegistered = true;
            if (cachedFunc.length > 0)
            {
                cachedFunc.forEach((f) => {
                    f && f();
                });
                cachedFunc.length = 0;
            }
            // fetchData();
        }).catch((error) => {
            // registration failed
            console.log('Registration failed with ' + error);
        });
    }
    else
    {
        console.log(' no service worker');
    }
}

function signUp(glb, e)
{
    let name = document.getElementById('name').value;
    let userName = document.getElementById('username').value;
    let password = document.getElementById('password').value;
    if (name == '' || userName == '' || password == '')
    {
        openDialog('missingRequire');
        return;
    }
    else
    {
        let res = services.pushUser({
            name,
            userName,
            password
        });
        if (res.code == 409)
            // alert('User already exists');
            openDialog('signUpFail');
            
        if (res.code == 200)
            openDialog('signUpSuccess');
        // location.href = '/signin/';
    }
}

function openDialog(name)
{
    console.log('click');
    let modal = document.getElementById(name);
    modal.style.display = "block";
}

window.addEventListener('load', (event) => {
    console.log('page is fully loaded');
    Initialize();
    registerWorker();
    document.onkeypress = function(e){
        if (e.keyCode == 13){
            document.getElementById('btnSignUp').click();
        }
    };
});