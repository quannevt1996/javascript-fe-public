const curVer = 'v1';

self.addEventListener('install', (event) => {
    event.waitUntil(
        caches.open('v1').then((cache) => {
            console.log('cache all');
            return cache.addAll([
                '/',
                '/signup/',
                '/signup/bundle.js',
                '/signin/',
                '/signin/bundle.js',
                '/index/',
                '/index/bundle.js',
                '/serviceWorker.js'
            ]);
        })
    );
});

self.addEventListener('fetch', (event) => {
    event.respondWith(
        caches.match(event.request).then((resp) => {
            // console.log('aaaa' + event.request);
            return resp || fetch(event.request).then((response) => {
                let responseClone = response.clone();
                caches.open('v1').then((cache) => {
                    cache.put(event.request, responseClone);
            });

            return response;
            }).catch(() => {
                return caches.match('./sw-test/gallery/myLittleVader.jpg');
            })
        })
    );
});

self.addEventListener('activate', (event) => {
    var cacheKeeplist = ['v1'];
    //CLear old cache handler (remark for later use)
    event.waitUntil(
        caches.keys().then((keyList) => {
            return Promise.all(keyList.map((key) => {
            if (cacheKeeplist.indexOf(key) === -1) {
                return caches.delete(key);
            }
            }));
        })
    );
});