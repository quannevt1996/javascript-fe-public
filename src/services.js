const helper = require('./helper');

var data;
var expiredTime = 60; // Minute(s)
var id = 0;
if (!localStorage.getItem('data') || localStorage.getItem('data') == '[]')
    data = [];
else 
    data = JSON.parse(helper.decryption(localStorage.getItem('data')));

var Response = function(code, msg, data = {}) {
    return {
        code,
        data,
        message: msg,
    }
};

function pushUser(user)
{
    let isUserExist = data.filter(d => d.userName == user.userName).length == 1 ? true : false;
    if (isUserExist)
    {
        return new Response(409, 'User already exists');
    }
    data.push({
        id: id++,
        ...user,
        time: new Date(), 
    });
    localStorage.setItem('data', helper.encryption(JSON.stringify(data)));
    return new Response(200, 'Register Completed');
}

function pushNote(token, note)
{
    let d = JSON.parse(helper.decryption(token));
    let user = findUserByName(d.userName);
    if (!user)
    {
        return new Response(401, 'Username not found');
    }
    else if (isTokenExpired(token))
    {
        return new Response(401, 'Token Expired');
    }
    else
    {
        user.time = new Date();
        if (!user.notes)
            user.notes = [];
        if (typeof(note) == Object)
            user.notes.push({
                date: new Date(),
                content: JSON.stringify(note)
            });
        else
            user.notes.push({
                date: new Date(),
                content: note + ''
            });
        data[data.indexOf(user)] = user;
        localStorage.setItem('data', helper.encryption(JSON.stringify(data)));
        return new Response(
            200,
            'Note added successfully',
            user
        );
    }
}

function logIn(userName, password)
{
    let user = findUserByName(userName);
    if (!user)
    {
        return new Response(401, 'Username not found');
    }
    else if (user.password != password)
    {
        return new Response(401, 'Incorrect Password');
    }
    else
    {
        if (isUserExpired(user))
        {
            user.time = new Date();
            data[data.indexOf(user)] = user;
            localStorage.setItem('data', helper.encryption(JSON.stringify(data)));
        }
        return new Response(
            200, 
            'Login successfully', 
            {
                token: helper.encryption(JSON.stringify({userName, time: user.time}))
            }
        );
    }
}

function logOut(token)
{
    let d = JSON.parse(helper.decryption(token));
    console.log(d);
    let user = findUserByName(d.userName);
    if (!user)
    {
        return new Response(401, 'Username not found');
    }
    else
    {
        user.time = new Date(0);
        data[data.indexOf(user)] = user;
        localStorage.setItem('data', helper.encryption(JSON.stringify(data)));
        // console.log(data[data.indexOf(user)]);
        return new Response(
            200,
            'User logged out',
            user
        );
    }
}

function getLogin(token)
{
    let userName = JSON.parse(helper.decryption(token)).userName;
    let time = JSON.parse(helper.decryption(token)).time;
    let user = findUserByName(userName);
    if (!user)
    {
        return new Response(401, 'Username not found');
    }
    else if (time != user.time)
    {
        return new Response(401, 'Invalid token');
    }
    else if (isTokenExpired(token))
    {
        console.log('token expired');
        return new Response(401, 'Token Expired');
    }
    else
    {
        return new Response(
            200, 
            'Login successfully', 
            {
                userName,
                token: helper.encryption(JSON.stringify({userName, time}))
            }
        );
    }
}

function getUser(token)
{
    let userName = JSON.parse(helper.decryption(token)).userName;
    let time = JSON.parse(helper.decryption(token)).time;
    let user = findUserByName(userName);
    if (!user)
    {
        return new Response(401, 'Username not found');
    }
    else if (isUserExpired(user))
    {
        return new Response(401, 'Token Expired');
    }
    else
    {

        return new Response(
            200, 
            'Get user successfully',
            user
        );
    }
}

function isTokenExpired(token)
{
    let time = JSON.parse(helper.decryption(token)).time;
    return ((new Date() - new Date(time))*1.67*1/100000 > expiredTime);
}

function isUserExpired(user)
{
    return (new Date() - new Date(user.time))*1.67*1/100000 > expiredTime;
}

function findUserByName(userName)
{
    return data.filter(d => d.userName == userName)[0];
}

function findUserById(id)
{
    return data.filter(d => d.id == id)[0];
}

module.exports = {
    pushUser,
    pushNote,
    logIn,
    logOut,
    getLogin,
    getUser
};