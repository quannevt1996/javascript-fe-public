# Dockerfile
# node version 12
FROM node:12
# set directory for work on docker
WORKDIR /iptp_test
# copy current package.json to working dir
COPY package.json /iptp_test
# install package
RUN npm install
# copy current file from server to docker container
COPY . /iptp_test
# open https port
EXPOSE 8080
# run node service
CMD npm run start
