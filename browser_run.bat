@echo off
IF EXIST node_modules (
echo Node module exists
) ELSE (
npm i
)
npm run start
pause